[toc]

### 介绍

基于 Vue3.2 的 ECharts5 简单入门案例。



### 项目构建步骤记录

1. 项目初始化

   ```sh
   npm init vite Vue3.2+ECharts5
   ```

2. 安装 Router

   ```sh
   npm i vue-router
   ```

3. 安装 Element Plus

   ```sh
   npm i element-plus
   ```

   1. 按需导入

      ```sh
      npm i -D unplugin-vue-components unplugin-auto-import
      ```

   2. 修改 vite.config.js

4. 安装 Axios

   ```sh
   npm i axios
   ```

5. 安装 ECharts

   ```sh
   npm i echarts
   ```

   

import { createRouter, createWebHashHistory } from 'vue-router'

export const routes = [
    {
        path: '/base1',
        name: 'Base1',
        component: () => import('../views/base1.vue')
    },
    {
        path: '/base2',
        name: 'Base2+Resize',
        component: () => import('../views/base2.vue')
    },
    {
        path: '/package1',
        name: 'Package1',
        component: () => import('../views/package1.vue')
    },
    {
        path: '/map',
        name: 'Map',
        component: () => import('../views/map.vue')
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router